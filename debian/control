Source: soundscaperenderer
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-buildinfo,
 libfftw3-dev,
 libjack-dev | libjack-jackd2-dev,
 libsndfile1-dev,
 libxml2-dev,
 pkg-config,
Build-Depends-Arch:
 libasio-dev,
 libecasoundc-dev,
 libqt5opengl5-dev [!armel !armhf],
Build-Depends-Indep:
 python3-sphinx,
 python3-sphinx-rtd-theme,
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: http://spatialaudio.net/ssr/
Vcs-Git: https://salsa.debian.org/multimedia-team/soundscaperenderer.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/soundscaperenderer

Package: soundscaperenderer-nox
Architecture: any
Depends:
 jackd,
 soundscaperenderer-common (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ecasound,
Description: tool for real-time spatial audio reproduction (without X support)
 The SoundScape Renderer (SSR) is a tool for real-time spatial audio
 reproduction providing a variety of rendering algorithms, e.g. Wave Field
 Synthesis, Higher-Order Ambisonics and binaural techniques.
 .
 This package contains a version of SSR compiled without X support.

Package: soundscaperenderer
Architecture: any
Depends:
 jackd,
 soundscaperenderer-common (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ecasound,
Description: tool for real-time spatial audio reproduction
 The SoundScape Renderer (SSR) is a tool for real-time spatial audio
 reproduction providing a variety of rendering algorithms, e.g. Wave Field
 Synthesis, Higher-Order Ambisonics and binaural techniques.
 .
 This package contains a version of SSR compiled with graphical interface using
 Qt.

Package: soundscaperenderer-common
Architecture: all
Multi-Arch: foreign
Breaks:
 soundscaperenderer (<< 0.5.0~dfsg-5~),
Replaces:
 soundscaperenderer (<< 0.5.0~dfsg-5~),
Depends:
 ${misc:Depends},
Recommends:
 soundscaperenderer | soundscaperenderer-nox,
Description: tool for real-time spatial audio reproduction (common-files)
 The SoundScape Renderer (SSR) is a tool for real-time spatial audio
 reproduction providing a variety of rendering algorithms, e.g. Wave Field
 Synthesis, Higher-Order Ambisonics and binaural techniques.
 .
 This package contains common files for all flavours of SSR.
