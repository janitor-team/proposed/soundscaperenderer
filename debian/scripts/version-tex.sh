#!/bin/sh

## this script creates a TeX-snippet to override the \today def
##  with the given version

VER=$1
if [ "x${VER}" = "x" ]; then
 VER=$(dpkg-parsechangelog -SVersion)
fi

echo "${VER}" \
| sed -e 's|~|$\\sim$|g' \
| sed -e 's|^\(.*\)$|\\def\\today{\1}|'

## \bf went missing...
echo '\\DeclareOldFontCommand{\\bf}{\\normalfont\\bfseries}{\\mathbf}'
